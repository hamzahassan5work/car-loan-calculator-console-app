                                 /*  -----------------------------------------
                                      CAR LOAN CALCULATOR - FINANCE A NEW CAR
                                     -----------------------------------------   */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include "loan.h"


using namespace std ;


int main ()
{
	
	loan car ;

	car.Login_File () ;

	char ch ;
do
  {
  	cout << "            ---------------------------------------------------------- \n"
  	     << "       *------| WELCOME TO CAR LOAN CALCULATOR - FINANCE A NEW CAR |------*    "
	     << "             ---------------------------------------------------------- \n\n" ;

	const int  Honda  = 1,
	           Suzuki = 2,
	           Toyota = 3 ;
	      int  selection ;


	cout << " Select the maker of a car \n"
	        " -------------------------- \n\n"
	        " 1: Honda   \n"
			" 2: Suzuki  \n"
			" 3: Toyota  \n\n" ;
	do
	 {
	    cout << " -Input your choice here: " ;
     	cin  >>   selection ;
	    cout <<   endl ;

	  if ( selection == 1  )
         {
      	  const float Civic = 1.1,
      	              City  = 1.2 ;
      	       double choice ;

		   cout << " Select the model of the Honda \n"
      	           " ------------------------------ \n\n"
      	           " 1.1: Civic \n"
      	           " 1.2: City  \n\n" ;
      	   do
		     {
		       cout << " -Input your choice here: " ;
      	       cin  >> choice ;
      	       cout << endl ;

		        if ( choice == 1.1 )
			       {
				      car.Honda_Civic () ;
				   }

		      else if  ( choice == 1.2 )
                       {
			        	  car.Honda_City () ;
                       }

		      else
		           {
		        	   cout << " \a *ERROR! 'You entered wrong choice, please again input your choice below' \n"
	        	               "             --------------------------------------------------               \n\n "  ;
	               }


             } while ( choice < 1.1 || choice > 1.2 ) ;
		 }

	  else if ( selection == 2 )
	          {
	        	const float Swift  = 2.1 ,
	        	            Mehran = 2.2 ;
	        	     double choice  ;

				cout << " Select the model of the Suzuki \n"
      	                " ------------------------------  \n\n"
      	                " 2.1: Swift   \n"
      	                " 2.2: Cultus  \n\n" ;

				do
				 {

      	             cout << " -Input your choice here: " ;
      	             cin  >> choice ;
					 cout << endl ;

	                    if ( choice == 2.1 )
			               {
                              car.Suzuki_Swift () ;
				           }


		          else if  ( choice == 2.2 )
                           {
			          	      car.Suzuki_Cultus () ;
		                   }

		          else
		                  {
		        	           cout << " \a *ERROR! 'You entered wrong choice, please again input your choice below' \n"
	        	                       "             --------------------------------------------------             \n\n "  ;
	                      }

                 } while ( choice < 2.1 || choice > 2.2 ) ;
	          }

	  else if ( selection == 3 )
	          {
	        	const float Corolla = 3.1,
      	                    Prado   = 3.2 ;
      	             double choice ;

		        cout << " Select the model of the Toyota \n"
      	                " ------------------------------  \n\n"
      	                " 3.1: Corolla \n"
      	                " 3.2: prado   \n\n" ;

      	        do
				 {

      	               cout << " -Input your choice here: " ;
      	               cin  >> choice ;
				       cout << endl ;

				        if ( choice == 3.1 )
			               {
				               car.Toyota_Corolla () ;
				           }

		          else if  ( choice == 3.2 )
                           {
			          	       car.Toyota_Prado () ;
		                   }

		          else
		                  {
		        	           cout << " \a *ERROR! 'You entered wrong choice, please again input your choice below' \n"
	        	                       "             --------------------------------------------------               \n\n "  ;
	                      }

			     } while ( choice < 3.1 || choice > 3.2 ) ;

	          }

	  else
	          {
	        	cout << " \a *ERROR! 'You entered wrong choice, please again input your choice below' \n"
	        	        "             --------------------------------------------------             \n\n "  ;
	          }

     } while ( selection < 1 || selection > 3  ) ;

     cout << "         Hey! Would You Like To Check Out This For another Car ? \n"
          << "                   -( Enter 'y' For Yes or 'n' For No )-          :-  " ;
     cin  >> ch ;

     while ( ch != 'y' && ch != 'n' )
           {
           	 cout << "\n\n\a            *ERROR! 'Incorrect Character - Again Input Here'  :-  " ;
			 cin  >> ch ;
           }

     if ( ch == 'y' )
        {
         system ("CLS") ;
        }

  } while ( ch == 'y' ) ;
  
    system ("CLS") ;

   cout << endl << endl << endl ;

   cout << "                 -------------THANk YOU------------- \n\n\n" ;

  return 0 ;
}

                                                // MAIN END



