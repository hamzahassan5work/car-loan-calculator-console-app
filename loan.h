
#include <string>
#include <limits>
using namespace std ;

#ifndef LOAN_H
#define LOAN_H

long double monthly_installments ;                 // Global Variable
fstream list ;                                     // Global Variable
ofstream outputfile ;                              // Global Variable
ifstream inputfile ;                               // Global Variable

class loan
{
   public:
   void Login_File () ;

   void Honda_Civic    () ;
   void Honda_City     () ;
   void Suzuki_Swift   () ;
   void Suzuki_Cultus  () ;
   void Toyota_Corolla () ;
   void Toyota_Prado   () ;

   void Car_Price ( long ) ;

   void duration_HBL ( int, long double, long ) ;
   void duration_MB  ( int, long double, long ) ;
   void duration_UBL ( int, long double, long ) ;
   void duration_AB  ( int, long double, long ) ;
   void duration_MCB ( int, long double, long ) ;
   void duration_SB  ( int, long double, long ) ;

   void HBL ( long double ) ;
   void MB  ( long double ) ;
   void UBL ( long double ) ;
   void AB  ( long double ) ;
   void MCB ( long double ) ;
   void SB  ( long double ) ;

   void resultfile_HBL ( int, long double, long ) ;
   void resultfile_MB  ( int, long double, long ) ;
   void resultfile_UBL ( int, long double, long ) ;
   void resultfile_AB  ( int, long double, long ) ;
   void resultfile_MCB ( int, long double, long ) ;
   void resultfile_SB  ( int, long double, long ) ;

   void file_HBL ( long double ) ;
   void file_MB  ( long double ) ;
   void file_UBL ( long double ) ;
   void file_AB  ( long double ) ;
   void file_MCB ( long double ) ;
   void file_SB  ( long double ) ;
} ;


void loan :: Login_File ()
{
   string name, username ;
   int numbers, password ;

   ifstream inputfile ;
   inputfile.open ( "login.txt" ) ;

   inputfile >> name ;

   cout << endl
        <<"                               ----------------- \n"
        <<"      #------------------------|  LOGIN SETUP  |-------------------------# \n"
		<<"                               ----------------- \n"    ;

   cout << endl << endl << endl;

   cout << "                       -Enter Username: " ;
   cin  >> username ;

   if ( username != name )
      {
      	cout << "\n\a                     *ERROR!  <Invalid Username>    \n\n"
      	     << "          -> Write Again -(Note! You Have Only Two Chances Left)  \n" ;
        cout << endl << endl ;

        cout << "                       -Enter Username: " ;
        cin  >> username ;
      }

    if ( username != name )
       {
        cout << "\n\a                     *ERROR!  <Invalid Username>    \n\n"
      	     << "          -> Write Again -(Note! You Have Only Last Chance Left)  \n" ;


        cout << endl << endl ;

        cout << "                       -Enter Username: " ;
        cin  >> username ;
       }

    if ( username != name )
       {
	     cout << "\n\a                     *ERROR!  <Invalid Username>    \n\n"  ;
	     system ("CLS") ;
      	 cout << "\n\n\a\a\a\a\a                          -> PROGRAM TERMINATED <-     \n\n" ;
         exit (0) ;
	   }


    cout << endl << endl ;

    inputfile >> numbers ;

    inputfile.close () ;

    cout << "                       -Enter Password: " ;
    cin  >> password ;

    if ( password != numbers )
       {
      	cout << "\n\a                     *ERROR!  <Invalid Password>    \n\n"
      	     << "          -> Write Again -(Note! You Have Only Two Chances Left)  \n" ;
        cout << endl << endl ;

        cout << "                       -Enter Password: " ;
        cin  >> username ;


         if ( password != numbers )
            {
               cout << "\n\a                     *ERROR!  <Invalid Password>    \n\n"
      	            << "          -> Write Again -(Note! You Have Only Last Chance Left)  \n" ;


               cout << endl << endl ;

               cout << "                       -Enter Password: " ;
               cin  >> username ;


                if ( password != numbers )
                   {
	                  cout << "\n\a                     *ERROR!  <Invalid Password>    \n\n"  ;
	                  system ("CLS") ;
      	              cout << "\n\n\a\a\a\a\a                          -> PROGRAM TERMINATED <-     \n\n" ;
                      exit (0) ;
	               }
            }
       }

    cout << endl << endl << endl ;
    cout << "                    -You have successfully succeed-\n\n\n\n                  "  ;


    system("pause") ;                    // to pause the screen at that moment and ask from user to continue...
	system("CLS") ;                      // to clear the screen

}

                                            // LOGIN FUNCTION END


void loan :: Honda_Civic ()
{
      	                   double sub_choice ;

		              cout << " Select the version of the Honda Civic \n"
      	                      " -------------------------------------- \n\n"
      	                      " 1.11: VTi 1.8 i-VTEC \n"
      	                      " 1.12: VTi Prosmatec 1.8 i-VTEC  \n\n" ;

	       do
		     {
					  cout << " -Input your choice here: " ;
      	              cin  >> sub_choice ;
					  cout << endl;

					    if ( sub_choice == 1.11 )
					       {
					       	  const long total_amount = 2000000 ;
					       	      cout << "          ------------------------------------------      \n "
								       << "  Wow! --| You choosed Honda Civic VTi 1.8 i-VTEC |--     \n "
								       << "         ------------------------------------------       \n\n "
								       << "           -> Its total amount is Rs: " << " " << total_amount
									   << "/-" << endl << endl ;

					       	 Car_Price ( total_amount ) ;
						   }

				  else  if ( sub_choice == 1.12 )
					       {
					        	  const long total_amount = 2400000 ;
					       	      cout << "          ----------------------------------------------------      \n "
								       << "  Wow! --| You choosed Honda Civic Prosmatec VTi 1.8 i-VTEC |--     \n "
								       << "         ----------------------------------------------------       \n\n "
								       << "           -> Its total amount is Rs: " << " " << total_amount
									   << "/-" << endl << endl ;

					       	 Car_Price ( total_amount ) ;
						   }

     				 else
	                                 {
	        	                       cout << " \a *ERROR! 'You entered wrong choice, please again input your choice below' \n"
	        	                               "             --------------------------------------------------        \n\n "  ;
	                                 }

             } while ( sub_choice < 1.11 || sub_choice > 1.12 ) ;

}

void loan :: Honda_City ()
{
                            double  sub_choice ;

		                    cout << " Select the version of the Honda City \n"
      	                            " -------------------------------------- \n\n"
      	                            " 1.21: i-VTEC \n"
                                    " 1.22: Aspire 1.5 i-VTEC  \n\n" ;

			       do
				     {

			                cout << " -Input your choice here: " ;
      	                    cin  >> sub_choice ;
				            cout << endl;

		           	       if ( sub_choice == 1.21 )
					       {
					       	  const long total_amount = 1600000 ;
					       	      cout << "          ---------------------------------      \n "
								       << "  Wow! --| You choosed Honda City i-VTEC |--     \n "
								       << "         ---------------------------------       \n\n "
								       << "           -> Its total amount is Rs: " << " " << total_amount
									   << "/-" << endl << endl ;

					       	  Car_Price ( total_amount ) ;
						   }

						  else if ( sub_choice == 1.22 )
					              {
					       	       const long total_amount = 1800000 ;
					       	       cout << "          --------------------------------------------      \n "
								        << "  Wow! --| You choosed Honda City Aspire 1.5 i-VTEC |--     \n "
								        << "         --------------------------------------------       \n\n "
								        << "           -> Its total amount is Rs: " << " " << total_amount
									    << "/-" << endl << endl ;

					                 Car_Price ( total_amount ) ;
						          }

                          else
	                                 {
	        	                       cout << " \a *ERROR! 'You entered wrong choice, please again input your choice below' \n"
	        	                               "             --------------------------------------------------        \n\n "  ;
	                                 }

                    } while ( sub_choice < 1.21 || sub_choice > 1.22 ) ;

}

void loan :: Suzuki_Swift ()
{
                                double  sub_choice ;

		                       cout << " Select the version of the Suzuki Swift \n"
      	                               " -------------------------------------- \n\n"
      	                               " 2.11: DLX 1.3 \n"
      	                               " 2.12: DLX 1.3 Automatic  \n\n" ;

                       do
					     {

					           cout << " -Input your choice here: " ;
      	                       cin  >> sub_choice ;
					           cout << endl;

							    if ( sub_choice == 2.11 )
					               {
					              	  const long total_amount = 1100000 ;
					       	          cout << "          ------------------------------------      \n "
								           << "  Wow! --| You choosed Suzuki Swift DLX 1.3 |--     \n "
								           << "         ------------------------------------       \n\n "
								           << "           -> Its total amount is Rs: " << " " << total_amount
									       << "/-" << endl << endl ;

					       	            Car_Price ( total_amount ) ;
						           }

				               else  if ( sub_choice == 2.12 )
					                    {
					        	           const long total_amount = 2400000 ;
					       	               cout << "          ----------------------------------------------      \n "
								                << "  Wow! --| You choosed Suzuki Swift DLX 1.3 Automatic |--     \n "
								                << "         ----------------------------------------------       \n\n "
								                << "           -> Its total amount is Rs: " << " " << total_amount
									            << "/-" << endl << endl ;

					       	               Car_Price ( total_amount ) ;
						                }

						       else
	                                 {
	        	                       cout << " \a *ERROR! 'You entered wrong choice, please again input your choice below' \n"
	        	                               "             --------------------------------------------------        \n\n "  ;
	                                 }

                        } while ( sub_choice < 2.11 || sub_choice > 2.12 ) ;

}

void loan :: Suzuki_Cultus ()
{
      	                         double  sub_choice ;

		                       cout << " Select the version of the Suzuki Cultus \n"
      	                               " -------------------------------------- \n\n"
      	                               " 2.21: EURO-2 \n"
                                       " 2.22: EURO-2 (CNG)  \n\n" ;

                        do
						  {

			                   cout << " -Input your choice here: " ;
      	                       cin  >> sub_choice ;
				               cout << endl;

		           	            if ( sub_choice == 2.21 )
					               {
					       	           const long total_amount = 700000 ;
					       	           cout << "          ------------------------------------      \n "
								            << "  Wow! --| You choosed Suzuki Mehran Euro-2 |--     \n "
								            << "         ------------------------------------       \n\n "
								            << "           -> Its total amount is Rs: " << " " << total_amount
									        << "/-" << endl << endl ;

					       	           Car_Price ( total_amount ) ;
						           }

				               else  if ( sub_choice == 2.22 )
					                    {
					        	           const long total_amount = 750000 ;
					       	               cout << "          ------------------------------------------      \n "
								                << "  Wow! --| You choosed Suzuki Mehran Euro-2 (CNG) |--     \n "
								                << "         ------------------------------------------       \n\n "
								                << "           -> Its total amount is Rs: " << " " << total_amount
									            << "/-" << endl << endl ;

					       	                Car_Price ( total_amount ) ;
						                }

						      else
	                                 {
	        	                       cout << " \a *ERROR! 'You entered wrong choice, please again input your choice below' \n"
	        	                               "             --------------------------------------------------        \n\n "  ;
	                                 }

                         } while ( sub_choice < 2.21 || sub_choice > 2.22 ) ;

}

void loan :: Toyota_Corolla ()
{
      	                          double  sub_choice ;

		                       cout << " Select the version of the Toyota Corolla \n"
      	                               " -------------------------------------- \n\n"
      	                               " 3.11: XLi VVTi \n"
      	                               " 3.12: GLi 1.3 VVTi  \n\n" ;

						do
						  {

					           cout << " -Input your choice here: " ;
      	                       cin  >> sub_choice ;
					           cout << endl;

					            if ( sub_choice == 3.11 )
					               {
					       	             const long total_amount = 1700000 ;
					       	             cout << "          ---------------------------------------      \n "
								              << "  Wow! --| You choosed Toyota Corolla Xli VVTi |--     \n "
								              << "         ---------------------------------------       \n\n "
								              << "           -> Its total amount is Rs: " << " " << total_amount
									          << "/-" << endl << endl ;

					       	             Car_Price ( total_amount ) ;
						           }

				              else  if ( sub_choice == 3.12 )
					                   {
					        	          const long total_amount = 1800000 ;
					       	              cout << "          -------------------------------------------      \n "
								               << "  Wow! --| You choosed Toyota Corolla GLi 1.3 VVTi |--     \n "
								               << "         -------------------------------------------       \n\n "
								               << "           -> Its total amount is Rs: " << " " << total_amount
									           << "/-" << endl << endl ;

					       	              Car_Price ( total_amount ) ;
						               }

						          else
	                                 {
	        	                       cout << " \a *ERROR! 'You entered wrong choice, please again input your choice below' \n"
	        	                               "             --------------------------------------------------        \n\n "  ;
	                                 }

                         } while ( sub_choice < 3.11 || sub_choice > 3.12 ) ;

}

void loan :: Toyota_Prado ()
{
      	                         double  sub_choice ;

		                       cout << " Select the version of the Toyota Prado \n"
      	                               " -------------------------------------- \n\n"
      	                               " 3.21: VX 3.0 \n"
                                       " 3.22: VX 4.0  \n\n" ;

							do
							  {

			                   cout << " -Input your choice here: " ;
      	                       cin  >> sub_choice ;
				               cout << endl;

								if ( sub_choice == 3.21 )
           					       {

					       	           const long total_amount = 4000000 ;
					       	           cout << "          -----------------------------------      \n "
								            << "  Wow! --| You choosed Toyota Prado VX 3.0 |--     \n "
								            << "         -----------------------------------       \n\n "
								            << "           -> Its total amount is Rs: " << " " << total_amount
									        << "/-" << endl << endl ;

                                         Car_Price ( total_amount ) ;
									 }


				               else  if ( sub_choice == 3.22 )
					                    {
					        	           const long total_amount = 4500000 ;
					       	               cout << "         -----------------------------------       \n"
								                << "  Wow! --| You choosed Toyota Prado VX 4.0 |--     \n"
								                << "         -----------------------------------     \n\n"
								                << "           -> Its total amount is Rs: " << " " << total_amount
									            << "/-" << endl << endl ;

					       	               Car_Price ( total_amount ) ;
						                }

						           else
	                                  {
	                                	cout << " \a *ERROR! 'You entered wrong choice, please again input your choice below' \n"
	        	                                "                 --------------------------------------------------     \n\n "  ;
	                                  }


							 } while ( sub_choice < 3.21 || sub_choice > 3.22 ) ;

}


void loan :: Car_Price ( long total_amount )
{

					       	      long double remaining_amount ;
					    		  float dp ;
								  long double down_payment ;
								  char ch ;
								  string filename, name ;

								  cout << " Please write its down payment here from 10 to 90 \n "
									   << " -( It will consider as a percentage )- \n\n"
									   << "  -Input Down Payment Here: " ;
								  cin  >> dp ;

								  while ( dp < 10 || dp > 90 )
								        {
								        	cout << endl ;
								        	cout << " \a ERROR! Write again in between 10 to 90 : " ;
								        	cin  >> dp ;
								        }

								  cout << endl << endl ;

      							  down_payment = ( dp * total_amount ) / 100 ;

      							  cout << setprecision(2) << fixed << showpoint ;

								  cout << " The down payment of your car is Rs: " << " " << down_payment << "/-"
								       <<   endl << endl ;

								  remaining_amount = (total_amount - down_payment) ;

								  cout << " Now the remaining amount of the car becomes is Rs: " << " " << remaining_amount
								       << "/-" << endl
								       << "     -( Which you have to pay in installments )-     " << endl << endl ;

								    const int min_years = 1 ;
								    const int max_years = 6 ;
								          int years ;

								  cout << " Please specify tenure in years for installments \n"
								       << "    ( NOTE! Min Years: 1 & Max Years: 6 )   \n\n"
									   << "  -Input Years Here: " ;
								   cin >> years ;
								  cout << endl ;

									 while( years < min_years || years > max_years )
								       {
								       	cout << " \a *ERROR! Wrong input, please try again from 1 to 6  \n\n"
										     << "  -Input Years Here: " ;
								         cin >> years ;
								       }

								             // RESULT FUNCTIONS

                                   duration_HBL ( years, remaining_amount, total_amount ) ;

                              if( dp > 45 )
                                {
                                   duration_MB  ( years, remaining_amount, total_amount ) ;
                                }
                            else
                                {
			                      cout << "                  -------------------------------              \n"
		  	                           << "                    | PROVIDED BY MEEZAN BANK |                \n"
                                       << "                  -------------------------------            \n\n"
                                       << "                 -> This bank does not give Loan when               \n\n"
                                       << "                    Down Payment is less then or equal to 45%       \n\n"
			                           << "                 -> This bank will give you a Loan when             \n\n"
			                           << "                    Down Payment is greater then 45%              \n\n\n" ;
                                }

                                   duration_UBL ( years, remaining_amount, total_amount ) ;

                              if( dp > 30 )
                                {
                                   duration_AB  ( years, remaining_amount, total_amount ) ;
                                }
                            else
                                {
			                      cout << "                  --------------------------------              \n"
		  	                           << "                    | PROVIDED BY ALFALAH BANK |                \n"
			                           << "                  --------------------------------            \n\n"
                                       << "                 -> This bank does not give Loan when               \n\n"
                                       << "                    Down Payment is less then or equal to 30%       \n\n"
			                           << "                 -> This bank will give you a Loan when             \n\n"
			                           << "                    Down Payment is greater then 30%              \n\n\n" ;
                                }

								   duration_MCB ( years, remaining_amount, total_amount ) ;

                              if( years < 4 )
                                {
								   duration_SB  ( years, remaining_amount, total_amount ) ;
                                }
                            else
                                {
			                      cout << "                  --------------------------------              \n"
		  	                           << "                    | PROVIDED BY SONERI BANK  |                \n"
			                           << "                  --------------------------------            \n\n"
                                       << "                 -> This bank does not give Loan when         \n\n"
                                       << "                    Years are greater then or equal to 4      \n\n"
                                       << "                 -> This bank will give you a Loan            \n\n"
                                       << "                    when Years are less then 4              \n\n\n"
                                       << "                       --------*****--------     		   \n\n\n"  ;
                                }



					cout << endl << endl << endl ;

					cout << " \a\a    -NOTE! --WOULD YOU LIKE TO SAVE ALL THESE RESULT IN A FILE--  \n "
                         << "                   -( Enter 'y' For Yes or 'n' For No )-         :-  " ;
	                cin  >> ch ;

                    while ( ch != 'y' && ch != 'n' )
                          {
           	                cout << "\n\n\a            *ERROR! 'Incorrect Character - Again Input Here'  :-  " ;
			                cin  >> ch ;
                          }

	               if ( ch == 'y' )
	                  {
	                        cin.ignore() ;
	                        INPUTAREA:
	                  	    cout << "\n\n                     -Enter File Name: " ;
	                  	    getline(cin, filename) ;

	                  	    inputfile.open("filenameslist.txt") ;

	                  	    while(inputfile >> name)
                            {
                                if(filename == name)
                                {
                                    cout << "\n\n\a             *ERROR! 'File Already Exist' - Try Again  \n" ;
                                    inputfile.close() ;
                                    goto INPUTAREA ;
                                }
                            }

                            inputfile.close();

	                  	    list.open("filenameslist.txt", ios::app | ios::out ) ;
	                  	    list << filename ;
	                  	    list << endl ;
	                  	    list.close() ;

	                  	    char *resultfile = &filename[0] ;
	                  	    
	                  	    outputfile.open(resultfile) ;

							resultfile_HBL ( years, remaining_amount, total_amount ) ;

                              if( dp > 45 )
                                {
							       resultfile_MB  ( years, remaining_amount, total_amount ) ;
                                }
                            else
                                {
			                      outputfile << "                  -------------------------------              \n"
		  	                                 << "                    | PROVIDED BY MEEZAN BANK |                \n"
                                             << "                  -------------------------------            \n\n"
                                             << "                 -> This bank does not give Loan when               \n\n"
                                             << "                    Down Payment is less then or equal to 45%       \n\n"
			                                 << "                 -> This bank will give you a Loan when             \n\n"
			                                 << "                    Down Payment is greater then 45%              \n\n\n" ;
                                }

                            resultfile_UBL ( years, remaining_amount, total_amount ) ;

                              if( dp > 30 )
                                {
							       resultfile_AB  ( years, remaining_amount, total_amount ) ;
                                }
                            else
                                {
			                      outputfile << "                  --------------------------------              \n"
		  	                                 << "                    | PROVIDED BY ALFALAH BANK |                \n"
			                                 << "                  --------------------------------            \n\n"
                                             << "                 -> This bank does not give Loan when               \n\n"
                                             << "                    Down Payment is less then or equal to 30%       \n\n"
			                                 << "                 -> This bank will give you a Loan when             \n\n"
			                                 << "                    Down Payment is greater then 30%              \n\n\n" ;
                                }

                            resultfile_MCB ( years, remaining_amount, total_amount ) ;

                              if( years < 4 )
                                {
							       resultfile_SB  ( years, remaining_amount, total_amount ) ;
                                }
                            else
                                {
			                      outputfile << "                  --------------------------------              \n"
		  	                                 << "                    | PROVIDED BY SONERI BANK  |                \n"
                                             << "                  --------------------------------            \n\n"
                                             << "                 -> This bank does not give Loan when         \n\n"
                                             << "                    Years are greater then or equal to 4         \n\n"
			                                 << "                 -> This bank will give you a Loan            \n\n"
			                                 << "                    when Years are less then 4           \n\n\n"
			                                 << "                       --------*****--------     			 \n\n\n"  ;

                                }



							outputfile.close() ;

		                    cout << "\n\n         --RESULT SUCCESSFULLY SAVED TO THE FILE NAME: " << filename ;
					    }

               cout << endl << endl << endl << endl << endl ;
}

void loan :: duration_HBL ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.35 * total_amount)/100 ;
								   	     HBL ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.35 * total_amount)/100 ;
								   	     HBL ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.35 * total_amount)/100 ;
								   	     HBL ( monthly_installments) ;
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.35 * total_amount)/100 ;
								   	     HBL ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.35 * total_amount)/100 ;
								   	     HBL ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.35 * total_amount)/100 ;
								   	     HBL ( monthly_installments );
								       }

}

void loan :: HBL ( long double monthly_installments )
{
	      cout << setprecision(2) << fixed << showpoint
		       << endl << endl
			   << "                      -----------------------              \n"
		  	   << "                        | PROVIDED BY HBL |                \n"
			   << "                      -----------------------            \n\n"
			   << "                 -> Processing fee is Rs: 5495/-         \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-\n\n\n" ;
}

                                                     // --------- //

void loan :: duration_MB ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.32 * total_amount)/100 ;
								   	     MB ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.32 * total_amount)/100 ;
								   	     MB ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.32 * total_amount)/100 ;
								   	     MB ( monthly_installments );
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.32 * total_amount)/100 ;
								   	     MB ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.32 * total_amount)/100 ;
								   	     MB ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.32 * total_amount)/100 ;
								   	     MB ( monthly_installments );
								       }
}

void loan :: MB ( long double monthly_installments )
{
          cout << setprecision(2) << fixed << showpoint
			   << "                  -------------------------------              \n"
		  	   << "                    | PROVIDED BY MEEZAN BANK |                \n"
			   << "                  -------------------------------            \n\n"
			   << "                 -> Processing fee is Rs: 5995/-             \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-\n\n\n" ;
}

                                                    // --------- //

void loan :: duration_UBL ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.39 * total_amount)/100 ;
								   	     UBL ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.39 * total_amount)/100 ;
								   	     UBL ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.39 * total_amount)/100 ;
								   	     UBL ( monthly_installments );
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.39 * total_amount)/100 ;
								   	     UBL ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.39 * total_amount)/100 ;
								   	     UBL ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.39 * total_amount)/100 ;
								   	     UBL ( monthly_installments );
								       }

}

void loan :: UBL ( long double monthly_installments )
{
          cout << setprecision(2) << fixed << showpoint
			   << "                      -----------------------              \n"
		  	   << "                        | PROVIDED BY UBL |                \n"
			   << "                      -----------------------            \n\n"
			   << "                 -> Processing fee is Rs: 4985/-         \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-\n\n\n" ;
}

                                                      // --------- //

void loan :: duration_AB ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.41 * total_amount)/100 ;
								   	     AB ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.41 * total_amount)/100 ;
								   	     AB ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.41 * total_amount)/100 ;
								   	     AB ( monthly_installments );
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.41 * total_amount)/100 ;
								   	     AB ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.41 * total_amount)/100 ;
								   	     AB ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.41 * total_amount)/100 ;
								   	     AB ( monthly_installments );
								       }

}

void loan :: AB ( long double monthly_installments )
{
          cout << setprecision(2) << fixed << showpoint
			   << "                  --------------------------------              \n"
		  	   << "                    | PROVIDED BY ALFALAH BANK |                \n"
			   << "                  --------------------------------            \n\n"
			   << "                 -> Processing fee is Rs: 4499/-              \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-\n\n\n" ;
}

                                                       // --------- //

void loan :: duration_MCB ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.375 * total_amount)/100 ;
								   	     MCB ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.375 * total_amount)/100 ;
								   	     MCB ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.375 * total_amount)/100 ;
								   	     MCB ( monthly_installments );
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.375 * total_amount)/100 ;
								   	     MCB ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.375 * total_amount)/100 ;
								   	     MCB ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.375 * total_amount)/100 ;
								   	     MCB ( monthly_installments );
								       }
}

void loan :: MCB ( long double monthly_installments )
{
          cout << setprecision(2) << fixed << showpoint
			   << "                      -----------------------              \n"
		  	   << "                        | PROVIDED BY MCB |                \n"
			   << "                      -----------------------            \n\n"
			   << "                 -> Processing fee is Rs: 6000/-         \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-\n\n\n" ;
}

                                                          // --------- //

void loan :: duration_SB ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.31 * total_amount)/100 ;
								   	     SB ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.31 * total_amount)/100 ;
								   	     SB ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.31 * total_amount)/100 ;
								   	     SB ( monthly_installments );
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.31 * total_amount)/100 ;
								   	     SB ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.31 * total_amount)/100 ;
								   	     SB ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.31 * total_amount)/100 ;
								   	     SB ( monthly_installments );
								       }
}

void loan :: SB ( long double monthly_installments )
{
          cout << setprecision(2) << fixed << showpoint
			   << "                  --------------------------------              \n"
		  	   << "                    | PROVIDED BY SONERI BANK  |                \n"
			   << "                  --------------------------------            \n\n"
			   << "                 -> Processing fee is Rs: 6595/-              \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-   \n\n\n"
			   << "                       --------*****--------     			  	                    \n\n\n"  ;
}
                                     //------------------------------//

											 //  filing process


void loan :: resultfile_HBL ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.35 * total_amount)/100 ;
								   	     file_HBL ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.35 * total_amount)/100 ;
								   	     file_HBL ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.35 * total_amount)/100 ;
								   	     file_HBL ( monthly_installments) ;
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.35 * total_amount)/100 ;
								   	     file_HBL ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.35 * total_amount)/100 ;
								   	     file_HBL ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.35 * total_amount)/100 ;
								   	     file_HBL ( monthly_installments );
								       }

}


void loan :: file_HBL ( long double monthly_installments )
{
 outputfile    << endl << endl
			   << "                      -----------------------              \n"
		  	   << "                        | PROVIDED BY HBL |                \n"
			   << "                      -----------------------            \n\n"
			   << "                 -> Processing fee is Rs: 5495/-         \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-\n\n\n" ;
}

                                                     // ********* //

void loan :: resultfile_MB ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.32 * total_amount)/100 ;
								   	     file_MB ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.32 * total_amount)/100 ;
								   	     file_MB ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.32 * total_amount)/100 ;
								   	     file_MB ( monthly_installments );
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.32 * total_amount)/100 ;
								   	     file_MB ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.32 * total_amount)/100 ;
								   	     file_MB ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.32 * total_amount)/100 ;
								   	     file_MB ( monthly_installments );
								       }
}

void loan :: file_MB ( long double monthly_installments )
{
 outputfile    << "                  -------------------------------              \n"
		  	   << "                    | PROVIDED BY MEEZAN BANK |                \n"
			   << "                  -------------------------------            \n\n"
			   << "                 -> Processing fee is Rs: 5995/-             \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-\n\n\n" ;
}

                                                     // ******** //

void loan :: resultfile_UBL ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.39 * total_amount)/100 ;
								   	     file_UBL ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.39 * total_amount)/100 ;
								   	     file_UBL ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.39 * total_amount)/100 ;
								   	     file_UBL ( monthly_installments );
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.39 * total_amount)/100 ;
								   	     file_UBL ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.39 * total_amount)/100 ;
								   	     file_UBL ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.39 * total_amount)/100 ;
								   	     file_UBL ( monthly_installments );
								       }

}

void loan :: file_UBL ( long double monthly_installments )
{
 outputfile    << "                      -----------------------              \n"
		  	   << "                        | PROVIDED BY UBL |                \n"
			   << "                      -----------------------            \n\n"
			   << "                 -> Processing fee is Rs: 4985/-         \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-\n\n\n" ;
}

                                                    // ********* //

void loan :: resultfile_AB ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.41 * total_amount)/100 ;
								   	     file_AB ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.41 * total_amount)/100 ;
								   	     file_AB ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.41 * total_amount)/100 ;
								   	     file_AB ( monthly_installments );
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.41 * total_amount)/100 ;
								   	     file_AB ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.41 * total_amount)/100 ;
								   	     file_AB ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.41 * total_amount)/100 ;
								   	     file_AB ( monthly_installments );
								       }

}

void loan :: file_AB ( long double monthly_installments )
{
   outputfile  << "                  --------------------------------              \n"
		  	   << "                    | PROVIDED BY ALFALAH BANK |                \n"
			   << "                  --------------------------------            \n\n"
			   << "                 -> Processing fee is Rs: 4499/-              \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-\n\n\n" ;
}

                                                 // ********* //

void loan :: resultfile_MCB ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.375 * total_amount)/100 ;
								   	     file_MCB ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.375 * total_amount)/100 ;
								   	     file_MCB ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.375 * total_amount)/100 ;
								   	     file_MCB ( monthly_installments );
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.375 * total_amount)/100 ;
								   	     file_MCB ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.375 * total_amount)/100 ;
								   	     file_MCB ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.375 * total_amount)/100 ;
								   	     file_MCB ( monthly_installments );
								       }
}


void loan :: file_MCB ( long double monthly_installments )
{
  outputfile   << "                      -----------------------              \n"
		  	   << "                        | PROVIDED BY MCB |                \n"
			   << "                      -----------------------            \n\n"
			   << "                 -> Processing fee is Rs: 6000/-         \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-\n\n\n" ;
}

                                            // ********* //

void loan :: resultfile_SB ( int years, long double remaining_amount, long total_amount )
{
                                	if ( years == 1 )
								       {
								   	     monthly_installments = (remaining_amount / 12) + (0.31 * total_amount)/100 ;
								   	     file_SB ( monthly_installments );
								       }

							   else if ( years == 2 )
								       {
								   	     monthly_installments = (remaining_amount / 24) + (0.31 * total_amount)/100 ;
								   	     file_SB ( monthly_installments );
								       }

							   else if ( years == 3 )
								       {
								   	     monthly_installments = (remaining_amount / 36) + (0.31 * total_amount)/100 ;
								   	     file_SB ( monthly_installments );
								       }

				               else if ( years == 4 )
								       {
								   	     monthly_installments = (remaining_amount / 48) + (0.31 * total_amount)/100 ;
								   	     file_SB ( monthly_installments );
								       }

							   else if ( years == 5 )
								       {
								   	     monthly_installments = (remaining_amount / 60) + (0.31 * total_amount)/100 ;
								   	     file_SB ( monthly_installments );
								       }

							   else if ( years == 6 )
								       {
								   	     monthly_installments = (remaining_amount / 72) + (0.31 * total_amount)/100 ;
								   	     file_SB ( monthly_installments );
								       }
}

void loan :: file_SB ( long double monthly_installments )
{
    outputfile << "                  --------------------------------              \n"
		  	   << "                    | PROVIDED BY SONERI BANK  |                \n"
			   << "                  --------------------------------            \n\n"
			   << "                 -> Processing fee is Rs: 6595/-              \n\n"
			   << "                 -> Monthly installments are Rs:  " << monthly_installments << "/-\n\n\n"
			   << "                       --------*****--------     			  	                    \n\n\n"  ;
}

#endif



